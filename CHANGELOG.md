# CHANGELOG

## Version : 0.3.1

- fix: notify : See merge request getto-systems-labo/version-dump!8


## Version : 0.3.0

- fix: merged commit message : See merge request getto-systems-labo/version-dump!7
- fix: releaseignore : See merge request getto-systems-labo/version-dump!6


## Version : 0.2.4

- add: prefix before merge commits : See merge request getto-systems-labo/version-dump!5

## Version : 0.2.3

- fix: .release-version-dump.sh : See merge request getto-systems-labo/version-dump!4


## Version : 0.2.2

- fix: readme : See merge request getto-systems-labo/version-dump!3


## Version : 0.2.1

- add: release ignore file : See merge request getto-systems-labo/version-dump!2


## Version : 0.2.0



## Version : 0.1.0


